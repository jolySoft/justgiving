﻿using GifAidCalculator.Service;
using GiftAidCalculator.Domain;
using Moq;
using NUnit.Framework;

namespace GiftAidCalculator.Tests.Service
{
    [TestFixture]
    public class CalculatorServiceTests
    {
        private CalculatorService _service;
        private Mock<IRepository<object>>  _repositoryMock;
        
        [SetUp]
        public void SetUp()
        {
            _repositoryMock = new Mock<IRepository<object>>();

            _service = new CalculatorService(20, _repositoryMock.Object);
        }

        [Test]
        public void CanCalculateAtInitalRate()
        {
            var donation = new Donation
            {
                Amount = 200,
                EventType = EventType.Other
            }; 
            
            var result = _service.Calculate(donation);

            Assert.That(result == 50);
        }

        [Test]
        public void CanOverloadTaxRate()
        {
            var donation = new Donation
                {
                    Amount = 200,
                    EventType = EventType.Other
                };
            _repositoryMock.Setup(repo => repo.Save(10));

            _service.SetNewRate(10);
            var result = _service.Calculate(donation);

            Assert.That(result == 22.22M);
            _repositoryMock.Verify(repo => repo.Save(10), Times.Once());
        }

        [Test]
        public void IsRoundedTo2DecimalPlaces()
        {
            // I made the asumption that bankers rounding
            // would be OK
            var donation = new Donation
            {
                Amount = 200,
                EventType = EventType.Other
            };

            _service.SetNewRate(30);
            var result = _service.Calculate(donation);

            Assert.That(result == 85.71M);            
        }

        [Test]
        public void RunningGetsExtra5Percent()
        {
            var donation = new Donation
                {
                    Amount = 200,
                    EventType = EventType.Running
                };

            var result = _service.Calculate(donation);

            Assert.That(result == 52.5M);
        }

        public void SwimmingOnlyGetsExtra3Percent()
        {
            var donation = new Donation
            {
                Amount = 200,
                EventType = EventType.Swimming
            };

            var result = _service.Calculate(donation);

            Assert.That(result == 51.5M);           
        }
    }
}