﻿using System;
using System.Collections.Generic;

namespace GifAidCalculator.Service
{
    public interface IRepository<TEntity>
    {
        TEntity Get(object Id);
        IEnumerable<TEntity> Get();
        void Save(TEntity entity);
        void Delete(TEntity entity);
    }
}