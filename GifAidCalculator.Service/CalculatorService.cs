﻿using System;
using GiftAidCalculator.Domain;

namespace GifAidCalculator.Service
{
    public class CalculatorService
    {
        private readonly IRepository<object> _repository;
        private int TaxRate { get; set; } 

        // Repository would be pumped in via IoC
        // The inital rate might will come froma config file 
        // Also pumped in via IoC using dynamic parameters
        public CalculatorService(int initialTaxRate, IRepository<object> repository)
        {
            _repository = repository;
            TaxRate = initialTaxRate;
        }

        public void SetNewRate(int newTaxRate)
        {
            _repository.Save(newTaxRate);
            TaxRate = newTaxRate;
        }

        public decimal Calculate(Donation donation)
        {
            var baseAmount = donation.Amount * (TaxRate / (100M - TaxRate));

            var supplementedAmount = AddSupplement(baseAmount, donation);

            return Math.Round(supplementedAmount, 2);
        }

        private decimal AddSupplement(decimal amount, Donation donation)
        {
            if (donation.EventType == EventType.Other) return amount;
            if (donation.EventType == EventType.Running) return (amount*0.05M) + amount;
            if (donation.EventType == EventType.Swimming) return (amount*0.03M) + amount;
            throw new InvalidOperationException("Unknown Event Type, can't calculate supplement");
        }
    }
}