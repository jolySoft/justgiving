﻿using System.Web.Http;
using GifAidCalculator.Service;
using GiftAidCalculator.Domain;
using Moq;

namespace GifAidCalculator.Api.Controllers
{
    public class GiftAidCalculatorController : ApiController
    {
        private CalculatorService _calculatorService;

        public GiftAidCalculatorController()
        {
            _calculatorService = new CalculatorService(20, new Mock<IRepository<object>>().Object);
        }

        [HttpGet]
        public decimal Get(decimal donationAmount, EventType eventType)
        {
            var donation = new Donation
            {
                Amount = donationAmount,
                EventType = eventType
            };

            return _calculatorService.Calculate(donation);
        }
    }
}
