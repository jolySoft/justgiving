﻿using System;
using GifAidCalculator.Service;
using GiftAidCalculator.Domain;
using Moq;

namespace GiftAidCalculator.TestConsole
{
	class Program
	{
		static void Main(string[] args)
		{
			// Calc Gift Aid Based on Previous
			Console.WriteLine("Please Enter donation amount:");
			Console.WriteLine(GiftAidAmount(decimal.Parse(Console.ReadLine())));
			Console.WriteLine("Press any key to exit.");
			Console.ReadLine();
		}

		static decimal GiftAidAmount(decimal donationAmount)
		{
			//This would all be under IoC, but hopefully you get the idea.
		    var repoMock = new Mock<IRepository<object>>();
		    var service = new CalculatorService(20, repoMock.Object);
		    var donation = new Donation
		        {
		            EventType = EventType.Other,
		            Amount = donationAmount
		        };

		    return service.Calculate(donation);
		}
	}
}
