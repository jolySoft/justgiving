﻿namespace GiftAidCalculator.Domain
{
    public enum EventType
    {
        Running,
        Swimming,
        Other
    }
}