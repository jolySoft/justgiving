﻿namespace GiftAidCalculator.Domain
{
    public class Donation
    {
        public decimal Amount { get; set; }
        public EventType EventType { get; set; }
    }
}